#
# Copyright (C) 2015 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

define Profile/SYSMOSOB
        NAME:=sysmocom SOB devices
        PACKAGES:=kmod-usb-core kmod-usb2
endef

define Profile/SYSMOSOB/Description
        Package set for sysmoSOB-AP
endef

$(eval $(call Profile,SYSMOSOB))

