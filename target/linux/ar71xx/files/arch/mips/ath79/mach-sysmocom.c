/*
 *  sysmocom devices
 *
 *  Copyright (C) 2015 Alexander Couzens <lynxis@fe80.eu>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 2 as published
 *  by the Free Software Foundation.
 */

#include <asm/mach-ath79/ath79.h>
#include <asm/mach-ath79/ar71xx_regs.h>
#include <linux/i2c.h>
#include <linux/i2c-algo-bit.h>
#include <linux/i2c-gpio.h>
#include <linux/platform_device.h>
#include "common.h"
#include "dev-eth.h"
#include "dev-gpio-buttons.h"
#include "dev-leds-gpio.h"
#include "dev-m25p80.h"
#include "dev-spi.h"
#include "dev-usb.h"
#include "dev-wmac.h"
#include "machtypes.h"

#define SYSMOCOM_GPIO_LED_WLAN		0
#define SYSMOCOM_GPIO_LED_ETH0		13
#define SYSMOCOM_GPIO_LED_PSE		15

#define SYSMOCOM_SOB_AP1_GPIO_I2C_SDA 18
#define SYSMOCOM_SOB_AP1_GPIO_I2C_SCL 19

#define SYSMOCOM_SOB_AP2_GPIO_I2C_SDA 26
#define SYSMOCOM_SOB_AP2_GPIO_I2C_SCL 27

#define SYSMOCOM_SOB_JB02V3_GPIO_LED_WLAN 0
#define SYSMOCOM_SOB_JB02V3_GPIO_LED_ETH0 17

#define SYSMOCOM_MAC0_OFFSET			0x0000
#define SYSMOCOM_MAC1_OFFSET			0x0006
#define SYSMOCOM_CALDATA_OFFSET			0x1000
#define SYSMOCOM_WMAC_MAC_OFFSET		0x1002

static const char *size_depend_part_probes[] = {
	"SizeDepend",
	NULL,
};

static struct flash_platform_data size_depend_flash_data = {
	.part_probes	= size_depend_part_probes,
};


static struct gpio_led sysmocom_sob_ap1_leds_gpio[] __initdata = {
	{
		.name		= "sysmocom:orange:wlan",
		.gpio		= SYSMOCOM_GPIO_LED_WLAN,
		.active_low	= 0,
	}, {
		.name		= "sysmocom:orange:eth0",
		.gpio		= SYSMOCOM_GPIO_LED_ETH0,
		.active_low	= 0,
	}, {
		.name		= "sysmocom:red:pse",
		.gpio		= SYSMOCOM_GPIO_LED_PSE,
		.active_low	= 0,
	}
};

static struct gpio_led sysmocom_sob_ap2_leds_gpio[] __initdata = {
	{
		.name		= "sysmocom:red:wlan",
		.gpio		= SYSMOCOM_GPIO_LED_WLAN,
		.active_low	= 0,
	}
};

static struct gpio_led sysmocom_sob_jb02_leds_gpio[] __initdata = {
	{
		.name		= "sysmocom:red:wlan",
		.gpio		= SYSMOCOM_GPIO_LED_WLAN,
		.active_low	= 0,
	}, {
		.name		= "sysmocom:red:system",
		.gpio		= SYSMOCOM_GPIO_LED_ETH0,
		.active_low	= 0,
	}
};

static struct gpio_led sysmocom_sob_jb02v3_leds_gpio[] __initdata = {
	{
		.name		= "sysmocom:red:wlan",
		.gpio		= SYSMOCOM_SOB_JB02V3_GPIO_LED_WLAN,
		.active_low	= 0,
	}, {
		.name		= "sysmocom:red:system",
		.gpio		= SYSMOCOM_SOB_JB02V3_GPIO_LED_ETH0,
		.active_low	= 0,
	}
};

static struct i2c_gpio_platform_data sob_ap1_i2c_gpio_data = {
	.sda_pin	= SYSMOCOM_SOB_AP1_GPIO_I2C_SDA,
	.scl_pin	= SYSMOCOM_SOB_AP1_GPIO_I2C_SCL,
	.udelay		= 20,
};

static struct i2c_gpio_platform_data sob_ap2_i2c_gpio_data = {
	.sda_pin	= SYSMOCOM_SOB_AP2_GPIO_I2C_SDA,
	.scl_pin	= SYSMOCOM_SOB_AP2_GPIO_I2C_SCL,
	.udelay		= 20,
};


static struct resource boot_counter_resources[] = {
	{
		.name	= "counter_base",
		.flags	= IORESOURCE_MEM,
		.start	= 0x1d000100,
		.end	= 0x1d000100 + 0x10 - 1,
	}
};

static struct platform_device sob_boot_counter = {
	.name		= "boot-counter-sram",
	.id		= 0,
	.resource       = boot_counter_resources,
	.num_resources  = ARRAY_SIZE(boot_counter_resources),
};

static struct platform_device sob_ap1_i2c_gpio_device = {
	.name		= "i2c-gpio",
	.id		= 0,
	.dev = {
		.platform_data = &sob_ap1_i2c_gpio_data,
	}
};

static struct platform_device sob_ap2_i2c_gpio_device = {
	.name		= "i2c-gpio",
	.id		= 0,
	.dev = {
		.platform_data = &sob_ap2_i2c_gpio_data,
	}
};

static struct i2c_board_info sob_ap_i2c_devs[] __initdata = {
	{
		I2C_BOARD_INFO("apds9300", 0x29),
	}, {
		I2C_BOARD_INFO("lm75b", 0x48),
	}, {
		I2C_BOARD_INFO("24c02", 0x50),
	},
};

static void __init sysmocom_common_setup(void)
{
	ath79_register_m25p80(&size_depend_flash_data);
	platform_device_register(&sob_boot_counter);
}

static void __init sysmocom_sob_ap1_setup(void)
{
	u8 *art = (u8 *) KSEG1ADDR(0x1fff0000);

	sysmocom_common_setup();
	ath79_register_wmac(art + SYSMOCOM_CALDATA_OFFSET,
			    art + SYSMOCOM_WMAC_MAC_OFFSET);

	ath79_setup_ar933x_phy4_switch(false, false);
	ath79_init_mac(ath79_eth1_data.mac_addr, art + SYSMOCOM_MAC1_OFFSET, 0);
	ath79_register_mdio(0, 0x0);
	/* ath79_register_eth(0) is unused because it using the internal switch */
	ath79_register_eth(1);

	ath79_gpio_function_disable(AR933X_GPIO_FUNC_ETH_SWITCH_LED0_EN |
				AR933X_GPIO_FUNC_ETH_SWITCH_LED1_EN |
				AR933X_GPIO_FUNC_ETH_SWITCH_LED2_EN |
				AR933X_GPIO_FUNC_ETH_SWITCH_LED3_EN |
				AR933X_GPIO_FUNC_ETH_SWITCH_LED4_EN |
				AR933X_GPIO_FUNC_UART_RTS_CTS_EN);

	ath79_register_leds_gpio(-1, ARRAY_SIZE(sysmocom_sob_ap1_leds_gpio),
				 sysmocom_sob_ap1_leds_gpio);
	ath79_register_usb();

	platform_device_register(&sob_ap1_i2c_gpio_device);
	i2c_register_board_info(0, sob_ap_i2c_devs,
				ARRAY_SIZE(sob_ap_i2c_devs));
}

static void __init sysmocom_sob_ap2_setup(void)
{
	u8 *art = (u8 *) KSEG1ADDR(0x1fff0000);

	sysmocom_common_setup();
	ath79_register_wmac(art + SYSMOCOM_CALDATA_OFFSET,
			    art + SYSMOCOM_WMAC_MAC_OFFSET);

	/* RJ45 port is separate WAN port / MAC, not switched with other ports */
	ath79_setup_ar933x_phy4_switch(false, false);
	ath79_init_mac(ath79_eth0_data.mac_addr, art + SYSMOCOM_MAC1_OFFSET, 0);
	ath79_init_mac(ath79_eth1_data.mac_addr, art + SYSMOCOM_MAC0_OFFSET, 0);
	ath79_register_mdio(0, 0x0);
	ath79_register_eth(1);
	ath79_register_eth(0);

	ath79_gpio_function_enable(AR933X_GPIO_FUNC_ETH_SWITCH_LED0_EN |
				AR933X_GPIO_FUNC_ETH_SWITCH_LED1_EN |
				AR933X_GPIO_FUNC_ETH_SWITCH_LED2_EN |
				AR933X_GPIO_FUNC_ETH_SWITCH_LED3_EN |
				AR933X_GPIO_FUNC_ETH_SWITCH_LED4_EN);

	ath79_gpio_function_disable(AR933X_GPIO_FUNC_UART_RTS_CTS_EN);

	ath79_register_leds_gpio(-1, ARRAY_SIZE(sysmocom_sob_ap2_leds_gpio),
				 sysmocom_sob_ap2_leds_gpio);
	ath79_register_usb();

	/* Enable GPIO26 and GPIO27 */
	ath79_reset_wr(AR933X_RESET_REG_BOOTSTRAP,
		       ath79_reset_rr(AR933X_RESET_REG_BOOTSTRAP) |
		       AR933X_BOOTSTRAP_MDIO_GPIO_EN);

	platform_device_register(&sob_ap2_i2c_gpio_device);
	i2c_register_board_info(0, sob_ap_i2c_devs,
				ARRAY_SIZE(sob_ap_i2c_devs));
}

static void __init sysmocom_sob_jb02_setup(void)
{
	u8 *art = (u8 *) KSEG1ADDR(0x1fff0000);

	sysmocom_common_setup();
	ath79_register_wmac(art + SYSMOCOM_CALDATA_OFFSET,
			    art + SYSMOCOM_WMAC_MAC_OFFSET);

	ath79_setup_ar933x_phy4_switch(false, false);
	ath79_init_mac(ath79_eth1_data.mac_addr, art + SYSMOCOM_MAC1_OFFSET, 0);
	ath79_register_mdio(0, 0x0);
	/* ath79_register_eth(0) is unused because it using the internal switch */
	ath79_register_eth(1);

	ath79_gpio_function_disable(AR933X_GPIO_FUNC_ETH_SWITCH_LED0_EN |
				AR933X_GPIO_FUNC_ETH_SWITCH_LED1_EN |
				AR933X_GPIO_FUNC_ETH_SWITCH_LED2_EN |
				AR933X_GPIO_FUNC_ETH_SWITCH_LED3_EN |
				AR933X_GPIO_FUNC_ETH_SWITCH_LED4_EN |
				AR933X_GPIO_FUNC_UART_RTS_CTS_EN);

	ath79_register_leds_gpio(-1, ARRAY_SIZE(sysmocom_sob_jb02_leds_gpio),
				 sysmocom_sob_jb02_leds_gpio);
	ath79_register_usb();
}

static void __init sysmocom_sob_jb02v3_setup(void)
{
	u8 *art = (u8 *) KSEG1ADDR(0x1fff0000);

	sysmocom_common_setup();
	ath79_register_wmac(art + SYSMOCOM_CALDATA_OFFSET,
			    art + SYSMOCOM_WMAC_MAC_OFFSET);

	/* RJ45 port is separate WAN port / MAC, not switched with other ports */
	ath79_setup_ar933x_phy4_switch(false, false);
	ath79_init_mac(ath79_eth0_data.mac_addr, art + SYSMOCOM_MAC1_OFFSET, 0);
	ath79_init_mac(ath79_eth1_data.mac_addr, art + SYSMOCOM_MAC0_OFFSET, 0);
	ath79_register_mdio(0, 0x0);
	ath79_register_eth(0);
	ath79_register_eth(1);

	ath79_gpio_function_disable(AR933X_GPIO_FUNC_ETH_SWITCH_LED0_EN |
				AR933X_GPIO_FUNC_ETH_SWITCH_LED1_EN |
				AR933X_GPIO_FUNC_ETH_SWITCH_LED2_EN |
				AR933X_GPIO_FUNC_ETH_SWITCH_LED3_EN |
				AR933X_GPIO_FUNC_ETH_SWITCH_LED4_EN |
				AR933X_GPIO_FUNC_UART_RTS_CTS_EN);

	ath79_register_leds_gpio(-1, ARRAY_SIZE(sysmocom_sob_jb02v3_leds_gpio),
				 sysmocom_sob_jb02v3_leds_gpio);
	ath79_register_usb();
}

MIPS_MACHINE(ATH79_MACH_SYSMOCOM_SOB_AP1, "SYSMOSOBAP1", "sysmocom SOB-AP1",
		sysmocom_sob_ap1_setup);

MIPS_MACHINE(ATH79_MACH_SYSMOCOM_SOB_AP2, "SYSMOSOBAP2", "sysmocom SOB-AP2",
		sysmocom_sob_ap2_setup);

MIPS_MACHINE(ATH79_MACH_SYSMOCOM_SOB_JB02, "SYSMOSOBJB02", "sysmocom SOB-JB02",
		sysmocom_sob_jb02_setup);

MIPS_MACHINE(ATH79_MACH_SYSMOCOM_SOB_JB02V3, "SYSMOSOBJB02V3", "sysmocom SOB-JB02V3",
		sysmocom_sob_jb02v3_setup);

